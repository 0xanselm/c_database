# Database Project

This is a simple database project implemented in C from scratch that utilizes **GoogleTest**<sup>1</sup>. The purpose of this project is to provide a basic database management system that can handle simple CRUD (Create, Read, Update, Delete) operations for a single table. The data is stored in a file on disk, and the user can interact with the database through a command-line interface.

### Features

- Create a new database
- Create a new table with specified fields
- Insert new records into the table
- Update existing records
- Delete records from the table
- Search for records by a given field
- List all records in the table

### Features to be completed

- Containerize with an Alpine Linux base image
- Extension of code generator via Flex and Bison

### Requirements

- C compiler (e.g. GCC)
- GNU Make
- POSIX-compliant system (Linux, Unix, macOS, etc.)

### Usage

To compile the program, run the following command:

```bash
make
```

This will create an executable file named `main` in the current directory. To run the program, execute the following command:

```bash
./main
```

This will start the database command-line interface. From here, you can use the following commands:

- `help` - Display a help message with available commands
- `create_database <database_name> - Create a new database with the specified name
- `create_table <table_name> <field1> <field2> ... <fieldN>` - Create a new table with the specified fields
- `insert <table_name> <field1_value> <field2_value> ... <fieldN_value>` - Insert a new record into the specified table
- `update <table_name> <field_name> <field_value> <update_field> <update_value>` - Update records in the specified table where the specified field matches the specified value
- `delete <table_name> <field_name> <field_value>` - Delete records from the specified table where the specified field matches the specified value
- `search <table_name> <field_name> <field_value>` - Search for records in the specified table where the specified field matches the specified value
- `list <table_name>` - List all records in the specified table
- `quit - Quit the database command-line interface

### Limitations

This database project is designed to be a simple proof-of-concept and has several limitations:

- It only supports a single table
- It only supports a fixed number of fields for each table
- It does not support indexing or searching by multiple fields

### Credits

This project was created by `0xAnselm` as a programming exercise inspired by, and extended from [2].

[1] https://github.com/google/googletest

[2] https://cstack.github.io/db_tutorial/
